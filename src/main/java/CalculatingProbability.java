import java.text.DecimalFormat;

public class CalculatingProbability {

    public double totalGuessQuantity;
    public double guestsKnowingGossip;
    public double guestsNotKnowingGossip;
    public double probabilityGossipToldToNewguest;
    public double totalProbability;

    public void getProbability(double totalGuessQuantity,double guestsKnowingGossip) {
        //probability of gossip to be told to not-knowing person is to divide all not-knowing


        probabilityGossipToldToNewguest =
                (totalGuessQuantity-guestsKnowingGossip)
                        /
                        (totalGuessQuantity-1);

        System.out.println(probabilityGossipToldToNewguest);

        totalProbability = probabilityGossipToldToNewguest*totalGuessQuantity;

        System.out.println(totalProbability);


    }

    public static void main(String[] args) {

//
        CalculatingProbability cp = new CalculatingProbability();
//        cp.getProbability(10,1);
        int totalGuessQuantity = 10;
        double probabilityAllWillHeartheGossip;

        for (int n = 1; n<=10; n++ ){

            cp.getProbability(totalGuessQuantity, n);

        }

    }



}
